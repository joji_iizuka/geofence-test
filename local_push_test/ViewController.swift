//
//  ViewController.swift
//  local_push_test
//
//  Created by 飯塚 譲二 on 2018/07/06.
//  Copyright © 2018年 飯塚 譲二. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

class ViewController: UIViewController, UNUserNotificationCenterDelegate,CLLocationManagerDelegate {
    
    var locationManager: CLLocationManager!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        // LocationManagerのインスタンスの生成
        locationManager = CLLocationManager()
        
        // LocationManagerの位置情報変更などで呼ばれるfunctionを自身で受けるように設定
        locationManager.delegate = self
        
        // 位置情報取得をユーザーに認証してもらう
        locationManager.requestAlwaysAuthorization()
        
        // モニタリング開始：このファンクションは適宜ボタンアクションなどから呼ぶ様にする。
        self.startGeofenceMonitering()
    }
    
    func startGeofenceMonitering() {
        
        // 位置情報の取得開始
        locationManager.startUpdatingLocation()
        
//        // モニタリングしたい場所の緯度経度を設定
//        let moniteringCordinate = CLLocationCoordinate2DMake(35.658581, 139.745433) // 東京タワーの緯度経度
//
//        // モニタリングしたい領域を作成
//        let moniteringRegion = CLCircularRegion.init(center: moniteringCordinate, radius: 1000.0, identifier: "TokyoTower")
//
//
//        // モニタリングしたい場所の緯度経度を設定
//        let tvTokyo = CLLocationCoordinate2DMake(35.6644924,139.7356062) // テレビ東京の緯度経度
//
//        // モニタリングしたい領域を作成
//        let tvTokyoRegion = CLCircularRegion.init(center: tvTokyo, radius: 500.0, identifier: "TVTokyo")
//
//        // モニタリング開始 max20個
//        locationManager.startMonitoring(for: moniteringRegion)
//        locationManager.startMonitoring(for: tvTokyoRegion)
        //モニタリングしたい場所の緯度経度を設定
        let moniteringCordinate = CLLocationCoordinate2DMake(35.625998, 139.723864) // 東京タワーの緯度経度

        // モニタリングしたい領域を作成
        let moniteringRegion = CLCircularRegion.init(center: moniteringCordinate, radius: 100.0, identifier: "五反田駅")
        
        locationManager.startMonitoring(for: moniteringRegion)
        dump(locationManager.monitoredRegions)
        
        locationManager.startMonitoringSignificantLocationChanges()
        //何m移動したらジオフェンスを再登録するか
        locationManager.distanceFilter = 100
    }
    
    func locationManager(_ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]){
        print("大幅に移動しました")
    }
    
    // ジオフェンスモニタリング
    
    // モニタリング開始成功時に呼ばれる
    func locationManager(_ manager: CLLocationManager, didStartMoniteringFor region: CLRegion) {
        print("モニタリング開始")
    }
    
    
    // モニタリングに失敗したときに呼ばれる
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("モニタリングに失敗しました。")
    }
    
    // ジオフェンス領域に侵入時に呼ばれる
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print(region.identifier+"に入りました。")
        // タイトル、本文、サウンド設定の保持
        let content = UNMutableNotificationContent()
        content.title = "設定したジオフェンスに入りました。"
        content.subtitle = "1秒経過しました"
        content.body = "タップしてアプリを開いてください"
        content.sound = UNNotificationSound.default()
        
        // seconds後に起動するトリガーを保持
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        // 識別子とともに通知の表示内容とトリガーをrequestに内包する
        let request = UNNotificationRequest(identifier: "Timer", content: content, trigger: trigger)
        
        // UNUserNotificationCenterにrequestを加える
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request) { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    // ジオフェンス領域から出たときに呼ばれる
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print(region.identifier+"から出ました。")
        // タイトル、本文、サウンド設定の保持
        let content = UNMutableNotificationContent()
        content.title = "設定したジオフェンスから出ました。"
        content.subtitle = "1秒経過しました"
        content.body = "タップしてアプリを開いてください"
        content.sound = UNNotificationSound.default()
        
        // seconds後に起動するトリガーを保持
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        // 識別子とともに通知の表示内容とトリガーをrequestに内包する
        let request = UNNotificationRequest(identifier: "Timer", content: content, trigger: trigger)
        
        // UNUserNotificationCenterにrequestを加える
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request) { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    // ジオフェンスの情報が取得できないときに呼ばれる
    func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
        print("モニタリングエラーです。")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // フォアグラウンドの場合でも通知を表示する
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
